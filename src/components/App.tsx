import React from 'react';
import { connect } from 'react-redux';
import { AppState } from '../store';

import '../main.css';

import { SystemState } from '../store/system/types';
import { updateSession } from '../store/system/actions';

import { ChatState } from '../store/chat/types';
import { sendMessage } from '../store/chat/actions';

import ChatHistory from './ChatHistory';
import ChatInterface from './ChatInterface';

import { thunkSendMessage } from '../thunks';

interface AppProps {
  sendMessage: typeof sendMessage;
  updateSession: typeof updateSession;
  chat: ChatState;
  system: SystemState;
  thunkSendMessage: any;
};

export type UpdateMessageParam = React.SyntheticEvent<{ value: string }>;

const App = (props: AppProps) => {
  const [ message, setMessage ] = React.useState({ message: '' });
  const { updateSession, sendMessage, thunkSendMessage, system, chat } = props;

  React.useEffect(() => {
    updateSession({
      loggedIn: true,
      session: 'my_session',
      userName: 'my_name'
    });
    sendMessage({
      user: 'Chat Bot',
      message: 'This is a very basic chat application written in TypeScript using React and Redux. Feel free to explore the source code.',
      timestamp: new Date().getTime()
    });
    thunkSendMessage('This message was sent by a thunk!');
  }, []);

  const updateMessage = (event: UpdateMessageParam) => {
    setMessage({ message: event.currentTarget.value });
  };

  const messageToBeSent = (message: string) => {
    sendMessage({
      user: system.userName,
      message,
      timestamp: new Date().getTime()
    });
    setMessage({ message: '' });
  }

  return (
    <div className="parent">
      <ChatHistory messages={chat.messages} />
      <ChatInterface
        userName={system.userName}
        message={message.message}
        updateMessage={updateMessage}
        sendMessage={messageToBeSent}
      />
    </div>
  );
}

const mapStateToProps = (state: AppState) => ({
  system: state.system,
  chat: state.chat
});

export default connect(
  mapStateToProps,
  { sendMessage, updateSession, thunkSendMessage }
)(App);