import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { sendMessage } from './store/chat/actions';
import { AppState } from './store';

export const thunkSendMessage = (
  message: string,
): ThunkAction<void, AppState, null, Action<string>> => async dispatch => {
  const asyncResponse = await exampleAPI();
  dispatch(
    sendMessage({
      message,
      user: asyncResponse,
      timestamp: new Date().getTime()
    })
  )
}

function exampleAPI() {
  return Promise.resolve("Async chat bot");
}